# +++++++++++++++++++++
# DON'T MOVE THIS FILE!
# +++++++++++++++++++++
#
# It is required to set the ZDOTDIR to a directory other than home
#
# See https://www.reddit.com/r/zsh/comments/3ubrdr/proper_way_to_set_zdotdir/
# for details

ZDOTDIR=$HOME/.zsh
. $ZDOTDIR/.zshenv
