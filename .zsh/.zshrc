#
# Executes commands at the start of an interactive session.
#

# Setup prezto
export ZDOTDIR="${HOME}/.zsh"

# export ZDOTDIR="${HOME}"
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Further customization

if [ -f /opt/homebrew/bin/brew ]; then
  eval "$(/opt/homebrew/bin/brew shellenv)"
else
  echo "Couldn't find brew. Things might not work"
fi


# Share history between all sessions
setopt inc_append_history  # save command before executing it
setopt share_history       # all sessions write to the same file

# disable auto_cd
unsetopt auto_cd

############### Syntax Highlighting #######################

# More eveident unmatched bracket
ZSH_HIGHLIGHT_STYLES[bracket-error]='bg=red'

# To define styles for nested brackets up to level 4
ZSH_HIGHLIGHT_STYLES[bracket-level-1]='fg=blue,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-2]='fg=red,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-3]='fg=yellow,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-4]='fg=magenta,bold'



############### PREZTO ##################


export EDITOR=nvim
export VISUAL=nvim

export CLICOLOR=1


################ PREZTO PROMPT THEME CUSTOMIZATION ###############

# # To customize prompt, run `p10k configure` or edit ~/.zsh/.p10k.zsh.
# [[ -f ~/.zsh/.p10k.zsh ]] && source ~/.zsh/.p10k.zsh


# -- CUSTOMIZE SEGMENTS IN PROMPTS (RIGHT AND LEFT)
# POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(vi_mode dir pyenv vcs custom_prompt_ready)
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir pyenv vcs newline vi_mode custom_prompt_ready)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs)

# customize icons
POWERLEVEL9K_PYTHON_ICON='\ue235'

# -- Custom segment to display a unique symbol at the end of the right prompt,
# -- so it can look different to the segment separators
# Use \ue0b0, \ue0b1, \ue0b2, \ue0b3 for "chevron" syumbols
# prefix with a backspace to remove the space that powerlevel9k adds before the symbol
POWERLEVEL9K_CUSTOM_PROMPT_READY='echo "\uf054"'
POWERLEVEL9K_CUSTOM_PROMPT_READY_BACKGROUND="black"
POWERLEVEL9K_CUSTOM_PROMPT_READY_FOREGROUND="green"

POWERLEVEL9K_MULTILINE_FIRST_PROMPT_GAP_CHAR='·'
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_GAP_FOREGROUND='green'
POWERLEVEL9K_LEFT_PROMPT_LAST_SEGMENT_END_SYMBOL=''
POWERLEVEL9K_RIGHT_PROMPT_FIRST_SEGMENT_START_SYMBOL=''

POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=$''
POWERLEVEL9K_RIGHT_SEGMENT_SEPARATOR=$''


# -- dir segment
POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_right"

# NOTE from https://github.com/bhilburn/powerlevel9k/issues/407#issuecomment-413181002
# In order to change the color of the entire 'dir' segment, all values related to 'dir' needs to be set.
POWERLEVEL9K_DIR_HOME_BACKGROUND='grey19'
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND='grey19'
POWERLEVEL9K_DIR_ETC_BACKGROUND='grey19'
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND='grey19'
POWERLEVEL9K_DIR_HOME_FOREGROUND='orangered1'
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND='orangered1'
POWERLEVEL9K_DIR_ETC_FOREGROUND='orangered1'
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND='orangered1'


# -- `vim_mode` segment color customization
POWERLEVEL9K_VI_INSERT_MODE_STRING="I"
POWERLEVEL9K_VI_COMMAND_MODE_STRING="%BN%b"

POWERLEVEL9K_VI_MODE_INSERT_BACKGROUND='yellow1'
POWERLEVEL9K_VI_MODE_INSERT_FOREGROUND='black'
POWERLEVEL9K_VI_MODE_NORMAL_BACKGROUND='deepskyblue3'
POWERLEVEL9K_VI_MODE_NORMAL_FOREGROUND='silver'


# -- vcs (git) segment customization
POWERLEVEL9K_VCS_FOREGROUND='black'
POWERLEVEL9K_VCS_BACKGROUND='green'
# POWERLEVEL9K_HIDE_BRANCH_ICON=true
POWERLEVEL9K_VCS_SHOW_SUBMODULE_DIRTY=false
POWERLEVEL9K_VCS_HIDE_TAGS=true
POWERLEVEL9K_VCS_GIT_HOOKS=(vcs-detect-changes git-aheadbehind git-remotebranch)


# -- pyenv segment color
POWERLEVEL9K_PYENV_BACKGROUND='orangered1'
POWERLEVEL9K_PYENV_FOREGROUND='black'



#################### MY STUFF #####################


# vim/nvim
export MYVIMRC="${HOME}/.vim/vimrc"

# the "magic" command. Use in place of `git` when dealing with the dotfiles repo
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotfiles config --local status.showUntrackedFiles no

if (( $+commands[git] )); then
  git config --global core.excludesfile $HOME/.gitignore_global
fi



#############################################
#               ALIASES
#############################################

if (( $+commands[nvim] )); then
  # should I link $HOME/init.vim to $MYVIMRC
  alias vim=nvim
else
  echo "NeoVim not found -- vim won't be aliased to nvim"
fi

# various useful aliases
alias whattheshell="ps -p $$"
alias rlp='source ${ZDOTDIR}/.zshrc && (tput setaf 4; echo "${ZDOTDIR}/.zshrc reloaded")'
alias edp='vim  ${ZDOTDIR}/.zshrc'
alias evrc='vim $MYVIMRC'
alias ls='ls -FGAp'
alias ll='ls -l'
alias lh='ls -lh'
alias la='ls -la'
alias lt='ls -lat'
alias lah='ls -lah'
alias lll='CLICOLOR_FORCE=1 ls -l | less -R'
alias clc='clear'
alias grep='grep --color=auto'

# prints $PATH, one element per line
alias path='echo -e ${PATH//:/\\n}'

alias fixaudio="sudo killall coreaudiod"

# improved `du`
alias du="ncdu --color dark -rr -x --exclude .git --exclude node_modules"

alias kittydark="kitten themes --reload-in=all Default"
alias kittylight="kitten themes --reload-in=all Ayu Light"

export GIT_EDITOR=nvim


# Add brew path ehead of system path
if (( $+commands[brew] )); then
  export BAT_THEME="TwoDark"
else
  echo "brew command not found -- skipping brew PATH prefix"
fi


# replace `cat` with `bat` (need to `brew install bat` first)
if (( $+commands[bat] )); then
  # should I link $HOME/init.vim to $MYVIMRC
  # alias bat='bat --theme TwoDark'
  export BAT_THEME="TwoDark"
  alias cat=bat
else
  echo "Bat not found -- skipping bat configuration"
fi


# coreutils (brew installed coreutils prepends commands with a 'g')
if (( $+commands[gshuf] )); then
  alias shuf=gshuf
fi

# replace `cat` with `bat` (need to `brew install bat` first)
if (( $+commands[bat] )); then
  alias cat='bat --theme TwoDark'
fi

if (( $+commands[fzf] )); then
  # replace `ctrl-r` (reverse history search) with fzf (need `brew install fzf`)
  alias preview="fzf --preview 'bat --color \"always\" {}'"
  # add support for ctrl+o to open selected file in VS Code
  export FZF_DEFAULT_OPTS="--bind='ctrl-o:execute(code {})+abort'"
fi

# better man
if (( $+commands[tldr] )); then
  alias help='tldr'
fi

# assuming ctags was installed with Brew
if (( $+commands[ctags] )); then
  alias ctags="`brew --prefix`/bin/ctags"
  # see https://gist.github.com/nazgob/1570678 for details
fi

# Configure FZF to use ripgrep (both can be brew installed)
if (( $+commands[fzf] )); then
  if type rg &> /dev/null; then
    export FZF_DEFAULT_COMMAND='rg --files'  # use rg for search
    export FZF_DEFAULT_OPTS='-m --height 50% --border --inline-info'
  fi
fi



# Google test colored output 
export GTEST_COLOR=yes


########################### PYENV ############################

# keep these lines towards the end of this file (the append to the PATH)
export PYENV_ROOT="$HOME/.pyenv"
ORIGINAL_PATH="$PATH"
export PATH="$PYENV_ROOT/bin:$PATH"
if (( $+commands[pyenv] )); then
  # FIXME: we should make sure this only runs if not run previously
  eval "$(pyenv init --path)"
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"
else
  echo "Pyenv not found -- skipping its configuration"
  export PATH="$ORIGINAL_PATH"
  unset PYENV_ROOT
fi

# make pyenv python installations install as framework (so matplotlib is happy)
export PYTHON_CONFIGURE_OPTS="--enable-framework CC=clang"


########################### Tcl-Tk ############################

# For Tcl-Tk (see https://stackoverflow.com/a/61879759)
export PATH="/opt/homebrew/opt/tcl-tk/bin:$PATH"

# These are only set to allow pyenv install python version with Tcl-Tk support
# They should probably be commented out in general
export LDFLAGS="-L/opt/homebrew/opt/tcl-tk/lib"
export CPPFLAGS="-I/opt/homebrew/opt/tcl-tk/include"
# For pkg-config to find tcl-tk
export PKG_CONFIG_PATH="/opt/homebrew/opt/tcl-tk/lib/pkgconfig"

# To actually install a new python version via pyenv (with support for tcl-tk)
# run e.g.:
# env PYTHON_CONFIGURE_OPTS="--with-tcltk-includes='-I/opt/homebrew/Cellar/tcl-tk/8.6.12_1/include' --with-tcltk-libs='-L/opt/homebrew/Cellar/tcl-tk/8.6.12_1/lib -ltcl8.6 -ltk8.6'" pyenv install 3.10.2


######################  Nvm related stuff  ######################

export NVM_DIR="$HOME/.nvm"
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion



# next line suggested by brew update
eval "$(fzf --zsh)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


########################### MY OWN bin ############################

export PATH=$HOME/bin:$PATH
