# NOTE: this file must be sourced (not executed as a script)
# E.g:
#
#   source setup_dotfiles.sh

[ -d "${HOME}/.dotfiles" ] && echo "Directory /path/to/dir exists. Remove it and re-try." && return

set -e

echo "Installing dotfiles."

# To avoid potential conflict with existing local files, we clone into a
# temporary folder. We will sync with home later
echo "[dotfiles] Cloning repo"
git clone \
  --recursive \
  --separate-git-dir=$HOME/.dotfiles \
	git@bitbucket.org:jorgeh/dotfiles.git tmpdotfiles

# zprezto is used as a submodule. In turn, it depends on more submodules (e.g. gitstatus)
echo "[dotfiles] Initializing and updating submodules"
pushd tmpdotfiles && git submodule sync ; popd
pushd tmpdotfiles && git submodule update --init --recursive ; popd
# update submodules to their latest commit
pushd tmpdotfiles && git submodule update --remote --merge ; popd

# Sync with home and remove temporary folder
echo "[dotfiles] Synchronizing tmpdotfiles with $HOME"
rsync --recursive --links --verbose --exclude '.git' tmpdotfiles/ $HOME/
echo "[dotfiles] Removing tmpdotfiles"
rm -Rf tmpdotfiles

# create dotfiles "command"
echo "[dotfiles] Creating dotfiles alias"
alias dotfiles="git --git-dir=${HOME}/.dotfiles/ --work-tree=$HOME"
dotfiles config --local status.showUntrackedFiles no

# reload zsh
echo "[dotfiles] Reloading zsh"
exec zsh

echo "------------------------"
echo "Dotfiles installed. Use the \`dotfiles\` command to manage them."
echo "------------------------"

set +e
