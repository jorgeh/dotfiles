# My dotfiles

Repo to port my unix custom environment anywhere.  Based on [this post](https://www.anand-iyer.com/blog/2018/a-simpler-way-to-manage-your-dotfiles.html) which in turn is based on [this thread](https://news.ycombinator.com/item?id=11070797).

---

** STOP: This is the Dotfiles repo Readme **
If you don't know what this means, *please don't edit this file*.

---


# Installation


## Pre-requisites and optional dependencies

- [required] Git
- [optional] tmux
  - awk
- [optional] Vim


## Installation

A setup shell script is provided for convenience. After downloading it, install the dotfiles by sourcing the downloaded script:

[To Do] make the `setup_dotfiles.sh` script a downloadable artifact

```sh
source setup_dotfiles.sh
```


## Installation walk-through

If running a shell script makes you nervous (that's understandable), here's a manual breakdown of steps the script executes.

0. **Preliminaries**

    The message at the top of the script is important. This script is not meant to be _executed_, but _**sourced**_. So before doing anything, we make sure `$HOME/.dotfiles` does not exist.

        # NOTE: this file must be sourced (not executed as a script)
        # E.g:
        #
        #   source setup_dotfiles.sh

        [ -d "${HOME}/.dotfiles" ] && echo "Directory /path/to/dir exists. Remove it and re-try." && return

        set -e

        ...

        echo "------------------------"
        echo "Dotfiles installed. Use the \`dotfiles\` command to manage them."
        echo "------------------------"

        set +e

    Also, you may wonder what that `set -e` command after checking for the non-existence of the `$HOME/.dotfiles/` directory is; it's simply a way to set the shell in a state that bails immediately if any command fails. There's a corresponding `set +e` at the end of the script, which simply returns the shell to it's default state.

    Now lets look at the "meat" of the script.

1. **Clone it**

    The first thing the script does is cloning this repo and initialize/update required submodules (e.g. zprezto)

        echo "Installing dotfiles."

        # To avoid potential conflict with existing local files, we clone into a
        # temporary folder. We will sync with home later
        echo "[dotfiles] Cloning repo"
        git clone \
          --recursive \
          --separate-git-dir=$HOME/.dotfiles \
        	git@bitbucket.org:jorgeh/dotfiles.git tmpdotfiles

        # zprezto is used as a submodule. In turn, it depends on more submodules (e.g. gitstatus)
        echo "[dotfiles] Initializing and updating submodules"
        pushd tmpdotfiles && git submodule sync ; popd
        pushd tmpdotfiles && git submodule update --init --recursive ; popd
        # update submodules to their latest commit
        pushd tmpdotfiles && git submodule update --remote --merge ; popd


2. **Sync contents to your $HOME**

    Next, the script carefully synchronizes the config files to your $HOME directory, so they are ready to be used by your system. Once that's done, it removed the cloned repo.

        # Sync with home and remove temporary folder
        echo "[dotfiles] Synchronizing tmpdotfiles with $HOME"
        rsync --recursive --links --verbose --exclude '.git' tmpdotfiles/ $HOME/
        echo "[dotfiles] Removing tmpdotfiles"
        rm -Rf tmpdotfiles

    _NOTE_: This will add several config files and directories (i.e. the contents of this repo) to your `$HOME` folder, replacing conflicting ones, so **backup previous files!** if case you are just experimenting.

3. **Create command**

    Then the script adds a `dotfiles` alias to your shell init script (e.g. `~/.bash_profile` or `~/.zshrc`). This alias can be use to check the VCS status (e.g. diff) of config files you may edit.

        # create dotfiles "command"
        echo "[dotfiles] Creating dotfiles alias"
        alias dotfiles="git --git-dir=${HOME}/.dotfiles/ --work-tree=$HOME"
        dotfiles config --local status.showUntrackedFiles no

4. **Reload zsh**

    Finally is necessary to reload zsh for the new config to be applied (note that is not sufficient to `source $HOME/.zsh/.zshrc`, as this won't apply the prompt theme. Instead, just run:

        # reload zsh
        echo "[dotfiles] Reloading zsh"
        exec zsh


# Usage

Use the alias created during the installation (`dotfiles`) as the solely way to update your config files, using git subcommands (e.g. `dotfiles pull` to bring the latest from the remote repo or `dotfiles add <new_dotfile> && dotfiles commit -m "commit message" && dotfiles push` to add a new file to the remote repo.

If needed, you can maintain different branches for OS/platform specific configs (e.g. `tag:MacOS`, `tag:Ubuntu`)

## Shell utilities

A few shell scripts are added:

- `tm`: tmux managing script [todo: expand]
- `colors.sh`: display colorful text, with some extra info. Good for testing color support
- `tm-theme`: change tmux's theme

# Notes

 - You should edit section `[user]` in `$HOME/.gitconfig` to use your git username details.

## Neovim

It is recommended to use Neovim in conjuction with `pyenv`. After installing `pyenv`, create a virtualenv based on
python3 named `neovim`. After activating that environment, run `pip install neovim black`


---
## To Dos

- Add `$HOME/config` dir and move tmux themes there
- Add Vim spell files (`:spellinfo`)
- Add custom snippets?
- much more!

---

## Test in docker

```sh
cd <root>/dotfiles

# only if needed
docker build -t mydotfiles:latest .

# launch a docker container
docker run --name DotfilesTest -it mydotfiles:latest
```

To resume:

```sh
docker start DotfilesTest
docker attach DotfilesTest
```

This should drop you in the contained running a zshell. There just type:

```sh
source /setup_dotfiles.sh
```
