BEGIN{
  # FS="="
  # print "wanted section", section
  active = 0
}
{
  # skip empty lines
  if ( NF == 0 ) next

  # skip comments
  if ( $0 ~ /^#.*/ ) next
  if ( $0 ~ /^;;.*/ ) next

  # process section line (e.g. `[section]`)
  if ( $0 ~ /^\[.*\]/) {

    # toggle read state if needed
    match($0, /^\[(.*)\]/, s)
    if ( s[1] == section) active = 1
    else active = 0

    # move onto next line
    next
  }

  if(active) {
    print $0
  }

}
