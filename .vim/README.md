# Vim config repo

**WARNING: customization freak ahead!**

## Usage


Make sure the to define an environment variable as follows:

    ```bash
    export MYVIMRC=${HOME}/.vim/vimrc
    ```


### NeoVim

I recently installed [NeoVim](https://neovim.io) to try it out. A simple way to
use this repo as the NeoVim config is to link `~/.vim/init.vim` to `.vim/vimrc`:

    ln -s ${HOME}/.vim/vimrc ${HOME}/.vim/init.vim


### First launch after cloning me

The first time you launch Vim, it should automatically install
[Vim Plug](https://github.com/junegunn/vim-plug), which is the plugin manager
of my choice. Only the manager is installed automatically. The plugins you need
to install from within Vim with a single command:

    :PlugInstall

I could have chosen to trigger this command automatically, but that would force
you to use all the plugins I've added to `vimrc`. You might want to tweak that
list before running the `:PlugInstall` command.

To update plugins, just run

    :PlugUpdate

If you remove pluings, is recommended to run

    :PlugClean


